import React from 'react'
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
  } from 'reactstrap';
class FormMasterData extends React.Component{
    render(){
        return(
        <Row>
          <Col xs="12" sm="6">
            <Card>
              <CardHeader>
                <strong>Employee</strong>
                <small> Form</small>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Name</Label>
                      <Input type="text" id="name" placeholder="Enter your name" required />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">NIP</Label>
                      <Input type="number" id="nip" placeholder="Enter your NIP" required />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Jabatan</Label>
                      <Input type="text" id="jabatan" placeholder="Position" required />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Email</Label>
                      <Input type="text" id="email" placeholder="Email" required />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Phone</Label>
                      <Input type="number" id="phone" placeholder="Phone Number" required />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Cabang</Label>
                      <Input type="text" id="cabang" placeholder="Cabang" required />
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>

              <Button onClick={() => {
                  alert('load....')
              }}>Submit</Button>
            </Card>
          </Col>
          <Col xs="12" sm="6">
            <Card>
              <CardHeader>
                <strong>Assignment</strong>
                <small> Form</small>
              </CardHeader>
              <CardBody>
                <FormGroup>
                  <Label htmlFor="company">Cabang</Label>
                  <Input type="text" id="cabang" placeholder="nama cabang" />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="vat">Manager</Label>
                  <Input type="text" id="manager" placeholder="manager name" />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="street">supervisor</Label>
                  <Input type="text" id="supervisor" placeholder="supervisor name" />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="street">staff</Label>
                  <Input type="text" id="staff" placeholder="staff name" />
                </FormGroup>
              </CardBody>
              <Button onClick={() => {
                  alert('load....')
              }}>Submit</Button>
            </Card>
          </Col>
        </Row>

        )
    }
}

export default FormMasterData